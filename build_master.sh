#!/bin/sh
if [ ! "${1}" = "skip" ] ; then
	./build_clean.sh
	./build_kernel_m340.sh CC='$(CROSS_COMPILE)gcc'
fi
rm arter97-kernel-"$(cat version)"*.zip 2>/dev/null
rm *.ko 2>/dev/null
cp m340.img kernelzip/boot.img
cd kernelzip/
7z a -mx0 arter97-kernel-"$(cat ../version)"-tmp.zip *
zipalign -v 4 arter97-kernel-"$(cat ../version)"-tmp.zip ../arter97-kernel-"$(cat ../version)"_frandom.zip
rm arter97-kernel-"$(cat ../version)"-tmp.zip
rm *.img
cd ..
if [ ! "${1}" = "skip" ] ; then
	cat ramdisk/arter97.sh | grep -v random > tmp_arter97.sh
	mv tmp_arter97.sh ramdisk/arter97.sh
	./build_kernel_m340.sh skip
	git checkout ramdisk/arter97.sh
	cp m340.img kernelzip/boot.img
	cd kernelzip/
	7z a -mx0 arter97-kernel-"$(cat ../version)"-tmp.zip *
	zipalign -v 4 arter97-kernel-"$(cat ../version)"-tmp.zip ../arter97-kernel-"$(cat ../version)".zip
	rm arter97-kernel-"$(cat ../version)"-tmp.zip
	rm *.img
	cd ..
fi
ls -al arter97-kernel-"$(cat version)"*.zip
