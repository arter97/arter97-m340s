#!/system/bin/sh

echo "ondemand" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo "90" > /sys/devices/system/cpu/cpu0/cpufreq/ondemand/up_threshold
chown system /sys/devices/system/cpu/cpu0/cpufreq/ondemand/sampling_rate
echo "20000" > /sys/devices/system/cpu/cpu0/cpufreq/ondemand/sampling_rate
echo "1008000" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
echo "122880" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
echo "fifo" > /sys/block/mmcblk0/queue/scheduler
echo "fifo" > /sys/block/mmcblk1/queue/scheduler
echo "256" > /sys/block/mmcblk0/bdi/read_ahead_kb
echo "1024" > /sys/block/mmcblk1/bdi/read_ahead_kb

busybox chmod 666 /dev/frandom /dev/erandom
busybox rm -f /dev/random /dev/urandom
busybox ln /dev/frandom /dev/urandom
busybox ln /dev/erandom /dev/random
busybox chmod 666 /dev/random /dev/urandom

sync

emmc_boot=`getprop ro.emmc`
case "$emmc_boot"
    in "1")
        chown system /sys/devices/platform/rs300000a7.65536/force_sync
        chown system /sys/devices/platform/rs300000a7.65536/sync_sts
    ;;
esac

while ! pgrep bootanimation ; do
	sleep 1
done

sleep 0.5

while pgrep bootanimation ; do
	sleep 1
done

export COUNT=0

while ! busybox ifconfig | grep eth0 ; do
	if [ $COUNT == "60" ]; then
		exit
	fi
	sleep 1
	COUNT=$(($COUNT + 1))
done

sleep 0.25

while busybox ifconfig | grep eth0 | grep 00:00:00:00:00:00 ; do
	sleep 0.5
	svc wifi disable
	sleep 0.5
	svc wifi enable
done

sleep 5
sync
echo 3 > /proc/sys/vm/drop_caches
